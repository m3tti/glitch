(use srfi-13
     srfi-1
     posix
     utils
     irregex)

(define rest cdr)

(define (disp data)
  (if (list? data)
      (display (string-concatenate data))
      (display data)))

(define (msg data)
  "shows a message for a given string or list"
  (disp data)
  (newline))

(define (help) 
  (msg (list "Welcome to glitch ticketing system"
	     "\n----"
	     "\ncommands:"
	     "\nnew\tcreates a new ticket"
	     "\n\tglitch new <ticket_name> <assignee> <text>"
	     "\n\nclose\tcloses a ticket"
	     "\n\tglitch close <ticket_number>"
	     "\n\nshow\tfilters for a given value"
	     "\n\tglitch show 'assignee: user'"
	     "\n\nhelp\tdisplays this help")))

(define (ticket-thunk data)
  "thunk used to write data"
  (lambda () (disp data)))

(define (gen-ticket-number)
  "finds the highest ticket number based on the filename in .tickets"
  "increments the highest value by one to gain the new ticket number"
  (let ((files (map (lambda (str) (string->number str)) (directory "./tickets")))
	(find-largest (lambda (a b) (if (> a b) a b))))
    (number->string (+ 1 (reduce find-largest 0 files)))))

(define (write-ticket filename thunk)
  "writes a ticket file"
  (let ((file (string-concatenate (list "./tickets/" filename))))
    (msg (list "writing ticket: " file))
    (with-output-to-file file thunk)))

(define (create-ticket title assignee status text)
  "creates a new ticket for the given arguments"
  "a list of three elements is needed: name, assignee, text"
  (let ((ticket-no (gen-ticket-number))
	(ticket  (list "ticket: " (gen-ticket-number)
		       "\ntitle: " title
		       "\nassignee: " assignee
		       "\nstatus: " status
		       "\n----\n"
		       text)))
    (msg ticket)
    (write-ticket ticket-no (ticket-thunk ticket))
    ))

(define (new-ticket args)
  "checks if all arguments are given to create a new ticket"
  (if (>= 3 (length args))
      (create-ticket (first args) (second args) "open" (third args))
      (msg "new needs 3 arguments: ticket name, assignee and text"))
  )

(define (load-content)
  "loads the content of all files in the ./tickets folder"
  (map (lambda (file)
	 (read-all (string-concatenate (list "./tickets/" file))))
       (directory "./tickets")))

(define (filter-tickets val)
  "filters all tickets for a given val"
  (let ((file-content (load-content))
	(filter-fn (lambda (str) (string-contains str val))))
    (msg (filter filter-fn file-content))
  ))

(define (show-tickets args)
  "shows a list filtered for a given value"
  (if (>= (length args) 1)
      (filter-tickets (first args))
      (msg "show needs one filter argument!")))

(define (close ticket-no)
  "closes a ticket by replacing the content of a given ticket"
  (let ((file-string (read-all (string-concatenate (list "./tickets/" ticket-no)))))
    (write-ticket
     ticket-no
     (ticket-thunk (irregex-replace "status: open" file-string "status: closed")))
    ))

(define (close-ticket args)
  "checks if all parameters are given"
  (if (>= 1 (length args))
      (close (first args))
      (msg "no ticket given")))
	
(define (dispatch fn data)
  "dispatches a given action"
  (apply fn (list data)))

(define (get-action action-name)
  "gets an action function for a given name"
  (cond
   ((string= action-name "new") new-ticket)
   ((string= action-name "close") close-ticket)
   ((string= action-name "show") show-tickets)
   ((string= action-name "help") (lambda (data) (help)))
   (else (lambda (data) (help)))))

(define (main args)
  (if (>= (length args) 1)
      (dispatch (get-action (first args)) (rest args))
      (help)
      ))

(when (not (directory? "./tickets"))
      (create-directory "./tickets"))

(define arguments (argv))
(main (cdr arguments))
